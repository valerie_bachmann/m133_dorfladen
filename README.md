### Installation
1. Den ganzen Ordner kopieren
2. Das Terminal öffnen und mittels ```cd```-Befehl in das Verzeichnis navigieren
3. Alle Abhängigkeiten welche in "package.json" definiert wurden per ```npm install``` installieren
(wenn Sie nur ```npm install``` ohnen einen konkreten Paketnamen ausführen, werden alle Abhängigkeiten installiert)

### Ausführen
1. Im Terminal die Applikation mittels ```npm start``` starten
2. Im Browser http://localhost:8080/ aufrufen
3. Zum Checkout navigieren -> Im Browser http://localhost:8080/checkout aufrufen

### Lizenz
[MIT](https://github.com/rollup/rollup/blob/master/LICENSE.md)